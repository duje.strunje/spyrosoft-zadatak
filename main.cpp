#include <iostream>
#include <thread>
#include <chrono>

#define NUM_STATES 3
#define START 65
#define SLEEP_SEC_MODE_SWITCH 2
#define SLEEP_SEC_PRINT 5

bool states[NUM_STATES];

void print_function(void)
{
	while (1) {
		for (int i = 0; i < NUM_STATES; i++) {
			std::cout <<  (char) (START + i) << ": ";
			if (states[i]) std::cout << "upaljeno";
			else std::cout << "izgaseno";
			std::cout << std::endl;
		}
		std::cout << "----------------" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(SLEEP_SEC_PRINT));
	}
}

void invert(int start, int end)
{
	bool new_value = not states[start];
	for (int i = start; i < end; i++) {
		states[i] = new_value;
		std::this_thread::sleep_for(std::chrono::seconds(SLEEP_SEC_MODE_SWITCH));
	}
}

int main(void)
{
	// postavi pocetno globalno stanje: sva stanja iskljucena
	for (int i = 0; i < NUM_STATES; i++) states[NUM_STATES] = false;

	// zaduzi jednu dretvu da ispisuje stanja svako SLEEP_SEC_PRINT sekundi
	std::thread print_thread(print_function);

	// upali sva stanja
	invert(0, NUM_STATES);

	while(1){
		char s;
		std::cin >> s;
		int index = s - START;
		//izgasi odabrano stanje i sva sljedeca i zatim ih opet upali
		invert(index, NUM_STATES);
		//simulacija javljanja zadnjeg prvom za reconnect i slanje naredbe paljenja svakom sljedećem upaljenom do prvog izgasenog
		std::this_thread::sleep_for(std::chrono::seconds(index));
		invert(index, NUM_STATES);
	}

	return 0;
}
