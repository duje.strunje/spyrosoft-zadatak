#include <iostream>
#include <thread>
#include <chrono>

#define NUM_STATES 3
#define START 65
#define SLEEP_SEC_MODE_SWITCH 2
#define SLEEP_SEC_PRINT 5

class Node {
private:
	bool working;
	Node *next_node;
	Node *main_node;
	int id;

public:
	Node(int id) : working(false), id(id) {}

	bool get_status() {
		return working;
	}

	void set_next(Node *node, Node *main) {
		next_node = node;
		main_node = main;
	}

	void reconnect() {
		if (working == false) working = true;
		std::this_thread::sleep_for(std::chrono::seconds(SLEEP_SEC_MODE_SWITCH));
		if (next_node != main_node) next_node->reconnect();
	}

	void turn_off() {
		working = not working;
		std::this_thread::sleep_for(std::chrono::seconds(SLEEP_SEC_MODE_SWITCH));
		if (next_node != main_node) next_node->turn_off();
		else main_node->reconnect();
	}


	void print() {
		if (id == 0) {
			std::this_thread::sleep_for(std::chrono::seconds(SLEEP_SEC_PRINT));
			std::cout << "----------------" << std::endl;
		}
		std::cout << char (id + START) << ": ";
		if (working) std::cout << "upaljeno";
		else std::cout << "izgaseno";
		std::cout << std::endl;
		next_node->print();
	}


};

int main(void)
{
	int count = 0;
	Node A(count++);
	Node B(count++);
	Node C(count++);

	A.set_next(&B, &A);
	B.set_next(&C, &A);
	C.set_next(&A, &A);

	A.reconnect();

	// zaduzi jednu dretvu da ispisuje stanja svako SLEEP_SEC_PRINT sekundi
	std::thread print_thread(&Node::print, A);

	while(1){
		std::string s;
		std::cin >> s;
		if (s == "A") A.turn_off();
		else if (s == "B") B.turn_off();
		else if (s == "C") C.turn_off();
		else std::cerr << "krivi uznos" << std::endl;
	}

	return 0;
}
